<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Question extends Model
{
    use HasFactory;
    protected $fillable = [
        'status',
        'subject_id',
        'level_id',
        'type_id',
        'rate',
        'fails',
        'success',
        'title',
    ];
}
